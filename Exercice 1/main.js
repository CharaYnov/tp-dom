document.addEventListener('DOMContentLoaded', () => {
    // -- your code here, after the DOM has been fully loaded
    let item = document.getElementsByClassName('item')
    var arr = Array.from(item);
    let compteurPink= 0
    let compteurRed =0
    arr.forEach(element => {

        var content = element.innerHTML.split(/>|</)
        content.forEach(elementContent => {
            if(elementContent === "cible 1"){
                element.classList.remove('light-grey')
                element.classList.add('pink')
                compteurPink++
            }
            if(elementContent === "cible 2"){
                element.classList.remove('light-grey')
                element.classList.add('red')
                compteurRed++
            }
        })


    });

    document.getElementsByClassName('target pink')[0].innerHTML =  '<h3>cible 1</h3> <span class="compteur">'+compteurPink+'</span>'
    document.getElementsByClassName('target red')[0].innerHTML =  '<h3>cible 2</h3> <span class="compteur">'+compteurRed+'</span>'
 })